//No. 1
function teriak() {
   console.log("Halo Sanbers!")
}
teriak()
console.log("----------------------------------")

//No. 2
function kalikan(num1, num2) {
    return num1 * num2
}

var hasilKali = kalikan(12, 4)
console.log(hasilKali)
console.log("----------------------------------")

//No. 3
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + (age.toString()) + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

console.log(introduce("Agus", 30, "Jln. Malioboro, Yogyakarta", "Gaming"))