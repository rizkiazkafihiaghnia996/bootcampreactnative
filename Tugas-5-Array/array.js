//Soal No. 1
console.log("---------------Nomor 1---------------")
function range(startNum, finishNum) {
    var numbers = []

    if(startNum > finishNum) {
        length = startNum - finishNum + 1
        for (var i = 0; i < length; i++) {
            numbers.push(startNum - i)
        }
    }
    else if(startNum < finishNum) {
        length = finishNum - startNum + 1
        for (var i = 0; i < length; i++) {
            numbers.push(startNum + i)
        }
    }
    else if(!startNum || !finishNum) {
        return -1
    }
    return numbers
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log("-------------------------------------")
console.log(" ")

//Soal No. 2
console.log("---------------Nomor 2---------------")
function rangeWithStep(startNum2, finishNum2, step) {
    var numbers2 = []

    if(startNum2 > finishNum2) {
        length2 = startNum2 - finishNum2 + 1
        for (var i = 0; i < length2; i+= step) {
            numbers2.push(startNum2 - i)
        }
    }
    else if(startNum2 < finishNum2) {
        length2 = finishNum2 - startNum2 + 1
        for (var i = 0; i < length2; i+= step) {
            numbers2.push(startNum2 + i)
        }
    }

    return numbers2
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log("-------------------------------------")
console.log(" ")

//Soal No. 3
console.log("---------------Nomor 3---------------")
function sum(startNum3, finishNum3, step2) {
    var numbers3 = []

    if(startNum3 > finishNum3 && !step2) {
        length3 = startNum3 - finishNum3 + 1
        for (var i = 0; i < length3; i++) {
            numbers3.push(startNum3 - i)
            const add = (a, b) => a + b
            var jumlahkan = numbers3.reduce(add)
        }
    }
    else if(startNum3 < finishNum3 && !step2) {
        length3 = finishNum3 - startNum3 + 1
        for (var i = 0; i < length3; i++) {
            numbers3.push(startNum3 + i)
            const add = (a, b) => a + b
            var jumlahkan = numbers3.reduce(add)
        }
    }
    else if(!startNum3 && !finishNum3 && !step2) {
        return 0
    }
    else if(startNum3 > finishNum3) {
        length3 = startNum3 - finishNum3 + 1
        for (var i = 0; i < length3; i+= step2) {
            numbers3.push(startNum3 - i)
            const add = (a, b) => a + b
            var jumlahkan = numbers3.reduce(add)
        }
    }
    else if(startNum3 < finishNum3) {
        length3 = finishNum3 - startNum3 + 1
        for (var i = 0; i < length3; i+= step2) {
            numbers3.push(startNum3 + i)
            const add = (a, b) => a + b
            var jumlahkan = numbers3.reduce(add)
        }
    }
    else if (!finishNum3) {
        return startNum3
    }
    return jumlahkan
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("-------------------------------------")
console.log(" ")

//Soal No. 4
console.log("---------------Nomor 4---------------")
function dataHandling(data) {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    if(data == 0001) {
        console.log("Nomor ID: " + input[0][0])
        console.log("Nama Lengkap : " + input[0][1])
        console.log("TTL: " + input[0][2] + ", " + input[0][3])
        console.log("Hobi: " + input[0][4])
        console.log(" ")
    }
    else if (data == 0002) {
        console.log("Nomor ID: " + input[1][0])
        console.log("Nama Lengkap : " + input[1][1])
        console.log("TTL: " + input[1][2] + ", " + input[1][3])
        console.log("Hobi: " + input[1][4])
        console.log(" ")
    }
    else if (data == 0003) {
        console.log("Nomor ID: " + input[2][0])
        console.log("Nama Lengkap : " + input[2][1])
        console.log("TTL: " + input[2][2] + ", " + input[2][3])
        console.log("Hobi: " + input[2][4])
        console.log(" ")
    }
    else if (data == 0004) {
        console.log("Nomor ID: " + input[3][0])
        console.log("Nama Lengkap : " + input[3][1])
        console.log("TTL: " + input[3][2] + ", " + input[3][3])
        console.log("Hobi: " + input[3][4])
        console.log(" ")
    }
}

console.log(dataHandling(0001))
console.log(dataHandling(0002))
console.log(dataHandling(0003))
console.log(dataHandling(0004))
console.log("-------------------------------------")
console.log(" ")

//Soal No. 5
console.log("---------------Nomor 5---------------")
function balikKata(str) {
    var newString = ""
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i]
    }
    return newString
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log("-------------------------------------")
console.log(" ")

//Soal No. 6
console.log("---------------Nomor 6---------------")
function dataHandling2() {
    var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
    input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA International Metro")
    console.log(input)
    
    var date = input[3]
    var bulan = date.split("/")
    
    var month = bulan[1]
    var month_int = parseInt(month)
    
    switch(month_int) {
        case 1: {console.log("Januari"); break;}
        case 5: {console.log("Mei"); break;}
        case 12: {console.log("Desember"); break;}
        default: {console.log("Bulan tidak dapat ditemukan!");}
    }

    var bulan_int = bulan.map(Number)
    bulan_int.sort(function(value1, value2){ return value2 - value1})
    var bulanInt_toStr = bulan_int.map(String)
    console.log(bulanInt_toStr)

    var bulan_join = bulan.join("-")
    console.log(bulan_join)

    var name = input[1]
    var slicedname = name.slice(0, 15)
    console.log(slicedname)
}
dataHandling2()

