//1). Looping While
console.log("LOOPING PERTAMA")
var number = 2
while(number < 21) {
    console.log(number + " - I love coding")
    number++
}

console.log("LOOPING KEDUA")
var number2 = 20
while(number2 > 1) {
    console.log(number2 + " - I will become a mobile developer")
    number2--
}
console.log("-------------------------------------------")

//2). Looping menggunakan For
for(var angka = 1; angka < 21; angka++) {
    if(angka %2 != 0 && angka %3 == 0) {
        console.log(angka + " - I Love Coding")
    }
    else if(angka %2 != 0) {
        console.log(angka + " - Santai")
    }
    else if(angka %2 == 0) {
        console.log(angka + " - Berkualitas")
    }
}
console.log("-------------------------------------------")

//3). Membuat Persegi Panjang
for (var i = 1; i < 5; i++) {
    console.log("########")
}
console.log("-------------------------------------------")

//4). Membuat Tangga
for(var i = 1; i < 8; i++) {
    console.log("#".repeat(i))
}
console.log("-------------------------------------------")

//5). Membuat Papan Catur
for(var i = 1; i < 9; i++) {
    if(i %2 != 0) {
        console.log(" # # # #")
    }
    if(i %2 == 0) {
        console.log("# # # # ")
    }
}
