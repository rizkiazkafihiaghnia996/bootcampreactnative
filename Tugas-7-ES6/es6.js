//No. 1
console.log("---------------Nomor 1---------------")
const golden = goldenFunction = () => {
    console.log("this is golden!")
}

golden()
console.log("-------------------------------------")
console.log(" ")

//No. 2
console.log("---------------Nomor 2---------------")
const newFunction = literal = (firstName, lastName) => {
    return {firstName, lastName, fullName: () =>{
        console.log(`${firstName} ${lastName}`)
    }
  }
}
newFunction("William", "Imoh").fullName()
console.log("-------------------------------------")
console.log(" ")

//No. 3
console.log("----------------------------------------Nomor 3----------------------------------------")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation)
console.log("---------------------------------------------------------------------------------------")
console.log(" ")

//No. 4
console.log("---------------Nomor 4---------------")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
var combined = [...west, ...east]
console.log(combined)
console.log("-------------------------------------")
console.log(" ")

//No. 5
console.log("-------------------------------------------------------Nomor 5-------------------------------------------------------")
const planet = "earth"
const view = "glass"
const a = "Lorem"
const b = "dolor sit amet,"
const c = "consectetur adipiscing elit,"
const d = "do eiusmod tempor"
const e = "incididunt ut labore et dolore magna aliqua. Ut enim"
const f = "ad minim veniam"

const before = `${view} ${a} ${b} ${planet} ${c} ${d} ${e} ${f}`

console.log(before)
console.log("---------------------------------------------------------------------------------------------------------------------")