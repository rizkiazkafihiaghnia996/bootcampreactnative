//if-else
console.log("-------------------if-else-------------------")
var name = "Bruce"
var peran = " "

if(name == " " && peran == " ") {
    console.log("Nama harus diisi!")
}
else if(name == " " && peran != " ") {
    console.log("Peran " + peran + " berhasil dipilih! Silahkan isi Nama.")
}
else if(name != " " && peran == " ") {
    console.log("Halo " + name + ", pilih peranmu untuk memulai game!")
}
else if(name != " " && peran == "Penyihir"){
    console.log("Halo " + peran + " " + name + ", kamu dapat melihat siapa yang menjadi werewolf!")
}
else if(name != " " && peran == "Guard"){
    console.log("Halo " + peran + " " + name + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
else if(name != " " && peran == "Werewolf"){
    console.log("Halo " + peran + " " + name + ", kamu akan memakan mangsa setiap malam!")
}
else {
    console.log("Peran tidak tersedia! Pilih 1 dari 3 peran berikut: Guard, Penyihir dan Werewolf.")
}
console.log(" ")

//Switch Case
console.log("-----------------Switch Case-----------------")
var hari = 21
var bulan = 1
var tahun = 1945

switch(hari) {
    case 1: {console.log("1"); break;}
    case 21: {console.log("21"); break;}
    case 31: {console.log("31"); break;}
    default: {console.log("Tanggal tidak dapat ditemukan!");}
}

switch(bulan) {
    case 1: {console.log("Januari"); break;}
    case 8: {console.log("Agustus"); break;}
    case 12: {console.log("Desember"); break;}
    default: {console.log("Bulan tidak dapat ditemukan!");}
}

switch(tahun) {
    case 1900: {console.log("1900"); break;}
    case 1945: {console.log("1945"); break;}
    case 2200: {console.log("2200"); break;}
    default: {console.log("Tahun tidak dapat ditemukan!");}
}
