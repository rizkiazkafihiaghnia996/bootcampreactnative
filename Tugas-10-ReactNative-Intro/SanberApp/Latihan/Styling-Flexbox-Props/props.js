import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

export default class App extends Component {
constructor(){
  super();
  this.state = {
    count : 3
  }
}

onPress (){
  this.setState({ count: this.state.count + 1 }); 
}

render(){
return (
 <View style={styles.container}>
      <View style={styles.countContainer}>
        <Text>Count: {this.state.count}</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => this.onPress()}
      >
        <Text>Press Here</Text>
      </TouchableOpacity>
    </View>
  );
}
}

const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: "center",
     paddingHorizontal: 10
   },
   button: {
     alignItems: "center",
     backgroundColor: "#DDDDDD",
     padding: 10,
     width: 250,
     height: 30,
     justifyContent: "center"
   },
   countContainer: {
     alignItems: "center",
     padding: 10
   }
 }); 