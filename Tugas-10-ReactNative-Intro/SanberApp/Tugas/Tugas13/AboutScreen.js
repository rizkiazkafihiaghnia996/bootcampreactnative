import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/FontAwesome'

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.statusBarSeparator} />
                <ScrollView>
                <View style={styles.body}>
                    <View style={styles.aboutMe}>
                        <Text style={styles.tentangSaya}>Tentang Saya</Text>
                        <TouchableOpacity>
                            <Icon style={styles.profilePicture} name="account-circle" size={180}/>
                        </TouchableOpacity>
                        <Text style={styles.namaSaya}>Rizki Azka Fihi Aghnia</Text>
                        <Text style={styles.jabatanSaya}>Rookie React Native Developer</Text>
                    </View>
                    <View style={styles.portfolioBar}>
                        <Text style={styles.title}>Portfolio</Text>
                        <View style={styles.titleLine} />
                        <View style={styles.portfolioContent}>
                            <TouchableOpacity>
                                <Icon style={styles.gitIcon} name='gitlab' size={40} />
                                <Text style={styles.gitTitle}>@rizki_azka</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Icon style={styles.gitIcon} name='github' size={40} />
                                <Text style={styles.gitTitle}>@azka_rizki</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.contactMeBar}>
                        <Text style={styles.title}>Hubungi Saya</Text>
                        <View style={styles.titleLine} />
                        <TouchableOpacity>
                            <View style={styles.facebook}>
                                <Icon2 style={styles.contactIcon} name='facebook-square' size={40} />
                                <Text style={styles.contactTitle}>Rizki Azka Fihi A</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.instagram}>
                                <Icon2 style={styles.contactIcon} name='instagram' size={40} />
                                <Text style={styles.contactTitle}>@rizki_a.f.a</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.twitter}>
                                <Icon2 style={styles.contactIcon} name='twitter' size={40} />
                                <Text style={styles.contactTitle}>@azka_rizki</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.spacer} />
                </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    statusBarSeparator: {
        height: 20,
        backgroundColor: 'white',
    },
    body: {
        flex: 1
    },
    aboutMe: {
        alignItems: 'center',
    },
    tentangSaya: {
        fontFamily: "Roboto",
        fontSize: 35,
        fontWeight: 'bold',
        color: '#003366',
        paddingTop: 20
    },
    profilePicture: {
        marginTop: -10,
        color: 'grey'
    },
    namaSaya: {
        fontFamily: "Roboto",
        fontSize: 18,
        fontWeight: 'bold',
        color: '#003366'
    },
    jabatanSaya: {
        fontFamily: "Roboto",
        fontSize: 13,
        fontWeight: 'bold',
        color: '#3EC6FF',
        paddingTop: 5
    },
    portfolioBar: {
        height: 110,
        backgroundColor: '#E5E5E5',
        borderRadius: 10,
        marginTop: 15,
        marginLeft: 9,
        marginRight: 9,
        flexDirection: 'column'
    },
    title: {
        fontSize: 16,
        color: '#003366',
        paddingLeft: 10,
        paddingTop: 3
    },
    titleLine: {
        backgroundColor: '#003366',
        height: 1.5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10
    },
    portfolioContent: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 5
    },
    gitIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        color: '#3EC6FF'
    },
    gitTitle: {
        color: '#003366',
        fontWeight: 'bold'
    },
    contactMeBar: {
        height: 190,
        backgroundColor: '#E5E5E5',
        borderRadius: 10,
        marginTop: 15,
        marginLeft: 9,
        marginRight: 9,
        flexDirection: 'column',
    },
    facebook: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    instagram: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    twitter: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    contactIcon: {
        paddingLeft: 100,
        paddingTop: 10,
        color: '#3EC6FF'
    },
    contactTitle: {
        paddingLeft: 20,
        paddingTop: 10,
        color: '#003366',
        fontWeight: 'bold'
    },
    spacer: {
        height: 15
    }
})