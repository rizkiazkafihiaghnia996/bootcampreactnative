import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native'

export default class LoginScreen extends Component {
    render() {
        return (
        <View style={styles.container}>
            <View style={styles.statusBarSeparator} />
            <ScrollView>
            <View style={styles.body}>
            <View style={styles.sanbercodeLogo}>
                <Image source={require('./images/logo.png')}/>
                <Text style={styles.login}>Login</Text>
            </View>
            <View style={styles.credentials}>
                <Text>Username/Email</Text>
                <TextInput style={styles.inputCredentials}/>
                <Text>Password</Text>
                <TextInput style={styles.inputCredentials}/>
            </View>
            <View style={styles.buttonMasuk}>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={styles.masuk}>Masuk</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.atau}>atau</Text>
                <TouchableOpacity>
                    <View style={styles.button2}>
                        <Text style={styles.daftar}>Daftar ?</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.spacer} />
            </View>
            </ScrollView>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    statusBarSeparator: {
        height: 10,
        backgroundColor: 'white',
    },
    body: {
        flex: 1
    },
    sanbercodeLogo: {
        paddingTop: 50,
        alignItems: 'center'
    },
    login: {
        fontFamily: 'Roboto',
        fontSize: 24,
        color: '#003366',
        paddingTop: 30
    },
    credentials: {
        paddingTop: 25,
        paddingLeft: 32,
        flexDirection: 'column',
        fontSize: 16,
        fontFamily: 'Roboto',
        color: '#003366'
    },
    inputCredentials: {
        height: 40,
        width: 294,
        borderColor: '#003366',
        borderWidth: 1,
        marginBottom: 15
    },
    button: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 10,
        marginLeft: 130
    },
    masuk: {
        paddingTop: 5,
        fontFamily: 'Roboto',
        color: 'white',
        fontSize: 16
    },
    atau: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#3EC6FF',
        marginLeft: 163,
        marginTop: 10,
        marginBottom: 4
    },
    button2: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: '#003366',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 10,
        marginLeft: 130
    },
    daftar: {
        paddingTop: 5,
        fontFamily: 'Roboto',
        color: 'white',
        fontSize: 16
    },
    spacer: {
        height: 15
    }
})