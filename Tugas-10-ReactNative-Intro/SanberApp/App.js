import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App'
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import RegisterScreen from './Tugas/Tugas13/RegisterScreen'
import Main from './Tugas/Tugas14/components/Main'
import Index from './Tugas/Tugas15/index'
import Index2 from './Quiz3/index'

export default function App() {
  return (
    //<YoutubeUI />
    //<LoginScreen />
    //<AboutScreen />
    //<RegisterScreen />
    //<Main />
    //<Index />
    <Index2 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
