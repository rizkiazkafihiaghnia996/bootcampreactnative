//No. 1
console.log("----------------------------------------Nomor 1----------------------------------------")
var dataArray = [
    ["Abduh", "Muhamad", "male", 1992],
    ["Ahmad", "Taufik", "male", 1985]
]
function arrayToObject(dataArray) {
    var now = new  Date()
    var thisYear = now.getFullYear()
    
    for(var i = 0; i < dataArray.length; i++) {
        if(!dataArray[i][3] || dataArray[i][3] > thisYear) {
            age = "Invalid Birth Year"
        }
        else {
            age = thisYear - dataArray[i][3]
        }

        var dataObject = {
            firstName : dataArray[i][0],
            lastName : dataArray[i][1],
            gender : dataArray[i][2],
            age : age
        }
        console.log(`${dataObject.firstName} ${dataObject.lastName} : ${JSON.stringify(dataObject)}`)
    }
}

arrayToObject(dataArray)
console.log("---------------------------------------------------------------------------------------")
console.log(" ")

//No. 2
console.log("----------------------------------------Nomor 2----------------------------------------")
function shoppingObject(memberID, money) {
    this.memberID = memberID
    this.money = money
    this.listPurchased = []
    this.changeMoney = 0
}

function shoppingTime(memberID, money) {
    if(!memberID || !money) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        var canAfford = true
        shopping = new shoppingObject(memberID, money)
        shopping.memberID = memberID
        shopping.money = money
        while(money > 0 && canAfford) {
            if(money >= 1500000) {
                shopping.listPurchased.push("Sepatu Stacattu")
                money -= 1500000
                canAfford = true
            }
            if(money >= 500000) {
                shopping.listPurchased.push("Baju Zoro")
                money -= 500000
                canAfford = true
            }
            if(money >= 250000) {
                shopping.listPurchased.push("Baju H&N")
                money -= 250000
                canAfford = true
            }
            if(money >= 175000) {
                shopping.listPurchased.push("Sweater Uniklooh")
                money -= 175000
                canAfford = true
            }
            if(money >= 50000) {
                shopping.listPurchased.push("Casing Handphone")
                money -= 50000
                canAfford = true
            }
            canAfford = false
        }
        shopping.changeMoney = money
        return shopping
    }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000))
console.log(shoppingTime("82Ku8Ma742", 170000))
console.log(shoppingTime("", 2475000))
console.log(shoppingTime("234JdhweRxa53", 15000))
console.log(shoppingTime())
console.log("---------------------------------------------------------------------------------------")
console.log(" ")

//No. 3
console.log("----------------------------------------Nomor 3----------------------------------------")
function naikAngkot(arrPenumpang) {
    rute = ["A", "B", "C", "D", "E", "F"]
    var biaya = 2000
    var endResult = []

    if(arrPenumpang.length == 0) {
        return arrPenumpang
    }

    for(var i = 0; i < arrPenumpang.length; i++) {
        var penumpangArray = arrPenumpang[i]
        var penumpangObject = {}

        penumpangObject.penumpang = penumpangArray[0]
        penumpangObject.naikDari = penumpangArray[1]
        penumpangObject.tujuan = penumpangArray[2]
        penumpangObject.bayar = biaya * (rute.indexOf(penumpangObject.tujuan) - rute.indexOf(penumpangObject.naikDari))
        
        endResult.push(penumpangObject)
    }
    return endResult
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))
console.log("---------------------------------------------------------------------------------------")
console.log(" ")